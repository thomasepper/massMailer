module Main where

import SendMail

main :: IO ()
main = mmailer =<< execParser opts
  where
    opts = info (parseArgs <**> helper)
         (  fullDesc
         <> progDesc "Sends our customized plain text mails via sendmail (or s/msmtp). The mail is defined as a mustache template with key-value pairs listed in a CSV of recipients."
         <> header "massMailer"
         )
