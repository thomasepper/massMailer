{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE DeriveGeneric     #-}

module SendMail
  ( module Options.Applicative
  , mmailer
  , parseArgs
  ) where

import           Prelude              hiding (putStrLn, readFile)
import           Data.Text.IO
import           Options.Applicative
import           Network.Mail.SMTP
import           Network.Mail.Mime
import           Data.Text                   (Text)
import qualified Data.Text                as T
import qualified Data.Text.Lazy           as LT
import qualified Data.ByteString.Lazy     as BL
import           Data.Csv             hiding (header, Parser)
import qualified Data.Vector              as V
import           GHC.Generics                (Generic)

data Config = Config
  { senderName  :: Text
  , senderEmail :: Text
  , subject     :: Text
  , template    :: String
  , keyValues   :: String
  , sendmailBin :: String
  }

data Recipient = Recipient
  { email :: !Text
  , link  :: !Text
  } deriving Generic

instance FromNamedRecord Recipient

parseArgs :: Parser Config
parseArgs = Config
            <$> strOption
                (  long "senderName"
                <> metavar "SENDER_NAME"
                <> help "Name of sender, e.g. \"Leonard McCoy\"."
                )
            <*> strOption
                (  long "senderEmail"
                <> metavar "SENDER_EMAIL"
                <> help "Email of sender, e.g. \"leonard.mccoy@example.org\"."
                )
            <*> strOption
                (  long "subject"
                <> metavar "SUBJECT"
                <> help "Subject line, e.g. \"Study invitation\"."
                )
            <*> strOption
                (  long "template"
                <> metavar "TEMPLATE"
                <> help "Mustache template containing the relevant keys, e.g. \"mail.template\"."
                )
            <*> strOption
                (  long "keyValues"
                <> metavar "KEY-VALUE-PAIRS"
                <> help "CSV with email addresses and variables parameterizing the fields, e.g. \"email,link\nleonard.mccoy@example.org,https://example.org/tokenNumber\"."
                )
            <*> strOption
                (  long "sendmailBin"
                <> metavar "SENDMAIL_BINARY"
                <> help "Sendmail binary path or symbolic link to msmtp, e.g. \"/bin/sendmail\"."
                <> showDefault
                <> value "~/.config/bin/sendmail"
                )

replaceTags :: Text -> Text -> Text
replaceTags templ link = T.replace "{{link}}" link templ

processKeyValues :: Text -> Recipient -> Config -> IO ()
processKeyValues templ p Config {..} = do
  putStrLn $ email p
  let body = replaceTags templ (link p)
      mail = simpleMail' (Address Nothing (email p))
                         (Address (Just senderName) senderEmail)
                         subject
                         (LT.fromStrict body)
  renderSendMailCustom sendmailBin ["-t"] mail

mmailer :: Config -> IO ()
mmailer cfg@(Config {..}) = do
  templ <- readFile template
  recp  <- BL.readFile keyValues
  case decodeByName recp of
    Left err     -> print err
    Right (_, v) -> V.forM_ v $ \p ->
      processKeyValues templ p cfg
