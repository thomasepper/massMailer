{ mkDerivation, base, bytestring, cassava, mime-mail
, optparse-applicative, smtp-mail, stdenv, text, vector
}:
mkDerivation {
  pname = "massMailer";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base bytestring cassava mime-mail optparse-applicative smtp-mail
    text vector
  ];
  executableHaskellDepends = [
    base bytestring cassava mime-mail optparse-applicative
  ];
  license = stdenv.lib.licenses.bsd3;
}
